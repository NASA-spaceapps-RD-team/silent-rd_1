const express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var fs = require('fs');

var record_data = JSON.parse(fs.readFileSync('public/silent-rd-locations.json', 'utf8'));


const app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.enable('trust proxy');

app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.get("/nasa/home", function(req,res){
	res.render("home");
});

app.get("/nasa/creativity", function(req,res){
	res.render("creativity");
});

app.get("/nasa/map", function(req,res){
	res.render("map", {records:record_data["RECORDS"]["RECORD"]});
});

app.get("/nasa/mission", function(req,res){
	res.render("mission");
});

app.get("/nasa/contact", function(req,res){
	res.render("contact");
});

app.listen(3000, function(){
	console.log('listening on port 3000');
});